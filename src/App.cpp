#include "cinder/app/App.h"
#include "cinder/gl/gl.h"
#include "cinder/app/RendererGl.h"
#include "cinder/Log.h"

using namespace ci;
using namespace app;

class AppApp : public ci::app::App {
public:
    void setup() override;
    void update() override;
    void draw() override;
};


void AppApp::setup(){

}

void AppApp::update(){
}

void AppApp::draw(){
    gl::clear(Color(0.5,0.5,0.5));

}


CINDER_APP( AppApp, RendererGl,[] ( App::Settings *settings ) {
	settings->setWindowSize( 1280, 720 );
	settings->setMultiTouchEnabled( false );
} )