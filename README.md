👋

This is a basic project setup for Cinder with CMake

It has everything necessary to get a project running.

Installing
======
* you'll need to download the [android_linux](https://github.com/cinder/Cinder/tree/android_linux) branch of Cinder. If you don't know how to install from github, follow 
[these instructions](https://libcinder.org/docs/guides/git/index.html)
* install CMake - I don't technically know which minimum version is required but I'm just going with the default thats mentioned in the branch.
* adjust the `CMakeLists.txt` file as necessary. On my OSX machine, I have Cinder in the `Documents` folder, inside of a folder called `libraries`. I renamed the folder containing
cinder to `cinder_linux`. 


Setup of source and headers
====
* As far as how to specify source files, currently I'm just adding them one by one as I go. 
* For header files, you can just specify the folder, it's not necessary to specify each header file.

References
======
* http://discourse.libcinder.org/t/rfc-cross-platform-cmake-support/102
* https://cmake.org/